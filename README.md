# graphql-queries

## Description
Just a repo of example graph-ql queries I've sandboxed on gitlab.com using the interactive explorer: https://gitlab.com/-/graphql-explorer

## Usage
https://docs.gitlab.com/ee/api/graphql/getting_started.html

## Disclaimer
The queries are provided for educational purposes. It is not supported by GitLab. 

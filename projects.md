# View all projects in a group with the lastActivityAt timestamp
#### lastActivityAt represents [project activity](https://docs.gitlab.com/ee/user/project/working_with_projects.html#view-project-activity)
#### [Video demonstration](https://vimeo.com/1001332254/228fcca777?share=copy) 

```
query {
  group(fullPath: "cguitarte-sandbox") {
    projects {
      nodes {
        name
        lastActivityAt
      }
    }
  }
}
```
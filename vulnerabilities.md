# get all Vulnerabilities in the Confirmed state, Critical severity and sorting by the date they were detected in ascending order for a specific project

```
query {
  project(fullPath: "group/project-path") {
    vulnerabilities(state: [CONFIRMED], severity: [CRITICAL], sort: detected_asc) {
      nodes {
        id
        title
        severity
        state
        updatedAt
        detectedAt
      }
    }
  }
}
```

# get all vulnerabilities and list any external issue links

```
query getVulnsWithIssues{
  project(fullPath: "group/project-path") {
    vulnerabilities {
      nodes {
        title
        severity
        state
        externalIssueLinks {
          nodes {
            externalIssue {
              title
              webUrl
              createdAt
            }
          }
        }
      }
    }
  }
}
```
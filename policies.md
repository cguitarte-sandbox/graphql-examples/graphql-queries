# View all projects linking to a security policy project
> fullPath == Security Policy Project

> nodes returned == projects linking to the Security Policy Project
```
query {
  project(fullPath: "cguitarte-sandbox/policies/test-copy-policy") {
    id
    securityPolicyProjectLinkedProjects {
      nodes {
        id
        name
        fullPath
      }
    }
  }
}
```

# View all namespaces/groups linking to a security policy project and their policies
```
{
  project(fullPath: "cguitarte-sandbox/policies/test-copy-policy") {
    id
    securityPolicyProjectLinkedNamespaces {
      nodes {
        id
        name
        fullPath
        approvalPolicies {
          nodes {
            name
          }
        }
        scanExecutionPolicies {
          nodes {
            name
          }
        }
      }
    }
  }
}
```

# View policies that are applied to projects via a linked security policy project

```
{
  project(fullPath: "cguitarte-sandbox/policies/test-copy-policy") {
    id
    securityPolicyProjectLinkedProjects {
      nodes {
        id
        name
        fullPath
        approvalPolicies {
          nodes {
            name
          }
        }
        scanExecutionPolicies {
          nodes {
            name
          }
        }
      }
    }
  }
}
```

# View scanExecutionPolicies assigned to projects in a namespace
> Note [pagination](https://docs.gitlab.com/ee/api/graphql/getting_started.html#pagination) if the number of projects in the namespace exceed 100
```
query {
  namespace(fullPath: "cguitarte-sandbox") {
    projects {
      nodes {
        id
        name
        description
        scanExecutionPolicies { nodes {
         name 
        }
        }
      }
    }
  }
}
```

# View approvalPolicies assigned to projects in a namespace
> Note [pagination](https://docs.gitlab.com/ee/api/graphql/getting_started.html#pagination) if the number of projects in the namespace exceed 100
```
query {
  namespace(fullPath: "cguitarte-sandbox") {
    projects {
      nodes {
        id
        name
        description
        approvalPolicies { nodes {
         name 
        }
        }
      }
    }
  }
}
```

# query count and linked projects

```
query {
  project(fullPath: "gitlab-org/govern/demos/sandbox/disable-semgrep-sep-security-policy-project") {
    securityPolicyProjectLinkedNamespaces {
      nodes {
        id
        name
        fullPath
      }
    }
    securityPolicyProjectLinkedProjects {
      count
      nodes {
        id
        name
        fullPath
      }
    }
  }
}
```
